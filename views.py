from flask import Blueprint
from celery import chain, group

from .main_celery.tasks import rentability_product, rentability_portfolio

bp = Blueprint('rentability', import_name=__name__)


def mount_products():
    products = []
    for i in range(100):
        products.append({
            'id': i,
            'name': f'product {i}'
        })

    portfolio_1 = dict(id=1, products=products[:25])
    portfolio_2 = dict(id=2, products=products[25:50])
    portfolio_3 = dict(id=3, products=products[50:75])
    portfolio_4 = dict(id=4, products=products[75:])

    return [portfolio_1, portfolio_2, portfolio_3, portfolio_4]


@bp.route('/', methods=['POST'])
def calculate_rentability():
    portfolio_list = mount_products()

    portfolio_tasks = []
    for portfolio in portfolio_list:
        
        products_tasks = []    
        for product in portfolio['products']:
            task = rentability_product.s(dict(
                portfolio_id=portfolio['id'],
                params=product,
                activity_type='product'
            ))
            products_tasks.append(task)

        if len(products_tasks):
            task = chain(group(products_tasks), rentability_portfolio.s(dict(
                portfolio_id=portfolio['id'],
                products=portfolio['products']
            )))
            portfolio_tasks.append(task)

    if len(portfolio_tasks):
        task = group(portfolio_tasks)
        task.apply_async()

    return dict(
        message='Calculo da rentabilidade iniciado...'
    ), 200
