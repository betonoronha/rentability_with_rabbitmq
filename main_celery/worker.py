import os
from celery import Celery


CELERY_TASKS = [
    'main_celery.tasks'
]

celery_app = Celery(
    'app',
    broker=os.getenv('RABBIT_MQ_URL')
)

# Load tasks
celery_app.autodiscover_tasks(CELERY_TASKS)

celery_app.conf.update(dict(
    CELERY_RESULT_BACKEND='db+{}'.format(os.getenv('SQLALCHEMY_DATABASE_URI')),
))
