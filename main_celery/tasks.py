from time import sleep

from .worker import celery_app


@celery_app.task(name="rentability_product", queue="rentability_product", acks_late=True)
def rentability_product(product_infos):
    sleep(2)
    print(product_infos)


@celery_app.task(name="rentability_portfolio", queue="rentability_portfolio", acks_late=True)
def rentability_portfolio(portfolio_id, products):
    sleep(5)
    print(f'===> fazendo portfolio: {portfolio_id}')
    print(products)