Running 
-------

1. Subir o rabbitmq e o postgres

    ```docker-compose up -d``` * acessa o postgres e criar um banco de dados chamado 'db_test'


1. Startar o worker de calculo do produto com -c = 5 (processos concorrentes)

    ```SQLALCHEMY_DATABASE_URI="postgresql://postgres:postgres@localhost:5432/db_test" RABBIT_MQ_URL="pyamqp://guest@localhost" celery -A main_celery.worker.celery_app worker -l info -Q rentability_portfolio -c 1```

2. Startar o worker de calculo do portfolio com -c = 1 (processos concorrentes)

    ```SQLALCHEMY_DATABASE_URI="postgresql://postgres:postgres@localhost:5432/db_test" RABBIT_MQ_URL="pyamqp://guest@localhost" celery -A main_celery.worker.celery_app worker -l info -Q rentability_portfolio -c 1```

3. Roda o app com flask e chama a rota 'localhost:5000/' (method POST)
